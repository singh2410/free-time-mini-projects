# Free time mini projects 
## In my free time just some fun by me
In python language

## First mini project- Rock,Paper & Scissors game
RPS Output-
![Screenshot_from_2021-05-25_03-35-58](/uploads/1f8ce1f8627fc49d2f93007650a50623/Screenshot_from_2021-05-25_03-35-58.png)
## Second mini project- ChoroplethMap
ChoroplethMap Output-
![Screenshot_from_2021-05-27_03-02-08](/uploads/b485655fb673d3c24b1a95f4839a0194/Screenshot_from_2021-05-27_03-02-08.png)
### Third mini project- Monty Hall Problem
Monty Hall Output-
![Screenshot_from_2021-05-28_01-51-06](/uploads/1ab7ef9340dc725edbf2ebb9c1b5c0dc/Screenshot_from_2021-05-28_01-51-06.png)
### Fourth mini project- 3D points and graphs
Output-
![Screenshot_from_2021-05-28_02-58-15](/uploads/87d26451559b07ac1bc83513661a57b2/Screenshot_from_2021-05-28_02-58-15.png)
